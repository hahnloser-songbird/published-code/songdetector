%%% Example code to train a network to detect a target syllable: %%%
%
% Example code explaining how to use the code to train a two-layer neural
% network to detect a unique time point (e.g. target syllable) in a
% spectrogram of a stereotyped song. This specific example is designed to
% use data from one day to train the network and apply it to another day to
% count the number of songs (as the number of target syllables). Default
% values are tuned for zebra finch songs and spectrograms with a buffersize
% of 4 ms.

% Example script to apply the different scripts for: 
%       - training the neural network
%       - applying the neural network 
%       - visualizing the network output and the detection points
%       - song number counting

% ---
% Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 
%
% Reference (please cite):
% Undirected singing rate as a non-invasive tool for welfare monitoring in 
% isolated male zebra finches
% Homare Yamahachi, Anja T. Zai, Ryosuke O. Tachibana, Anna E. Stepien, 
% Diana I. Rodrigues, Sophie Cave-Lopez, Corinna Lorenz, Ezequiel Arneodo, 
% Nicolas Giret, Richard H. R. Hahnloser
% DOI: 10.1371/journal.pone.0236333
%
% Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
% ETH Zurich and University of Zurich
% Website: https://gitlab.ethz.ch/songbird/songdetector
%
% Author: Richard H.R. Hahnloser and colleagues             Revision 0.0.0
% Author: Anja T. Zai            Date: 18-June-2020         Revision 1.0.0
%

%% load the training data:
% Input required to train the neural network: 
%
% SPEC      -   SPEC is a 1xN dimensional cell array (N = number of 
%               individual spectrograms) where each entry contains one
%               spectrogram of size SxT. The number of frequency bins S has 
%               to be the same for all spectrograms SPEC{n} whereas the 
%               duration T of each spectrogram can vary.
%
% pos_bins  -   POS_BINS is a 1xN dimensional cell array of positive 
%               examples for training the network. It specifies the bin IDs 
%               of a unique point in the song (e.g. onset of a target  
%               syllable). POS_BINS{n} has the length of the number of  
%               occurences of this unique point in the spectrogram SPEC{n}. 
%               Thus if the unique point occurs inthe 100th and the 250th 
%               bin in SPEC{n} then POS_BINS{n} = [100 250], if there are 
%               no occurances then POS_BINS{n} = []. It is important that 
%               POS_BINS is as clean as possible:
%               - as few false positives (bins specified in POS_BINS that 
%                 do not correspond to the unique position, big impact on 
%                 performance) and 
%               - as few false negatives (unique points that are not listed
%                 in POS_BINS, less impact on performance) as possible. 
%

% add path
addpath('internal')
addpath('example_data')

% load data (one day from bird b14g16): 'SPEC', 'pos_bins'
load('SPEC-b14g16-day1.mat')

% Optional: A single spectrogram of cage noise (SxT dimensional matrix) to 
% be added to the negative examples used to train the neural network. This
% input is not required to train the network, but improves the network's
% performance (reduces false detection of cage noise). Ideally, this cage 
% noise was recorded with the same recording system and in the same cage
% as the song of the bird.  
%
% Load noise spectrogram for b14g16: 'noise'
load('noise')

% browse through different expample spectrograms
gID = net_plot_for_song_count(SPEC,pos_bins);

%% train two-layer neural network:
% Function to train the neural network to detect the training data
% pos_bins. To display informations about the function and its
% inputs/outputs (same for other functions) 
help net_train_for_song_count
% Here, the neural network is trained to detect the training data pos_bins
% using a window of 25 bins starting at the points defined in pos_bins
% ('win',[0 24])
[net, param] = net_train_for_song_count(SPEC, pos_bins,'win',[0 24],'noise',noise);

% Now feel free to inspect the network output using the two new figures 
% and the interactive figure below.  
% Here, you find a description of the figures and how they can be used to
% evaluate the training success. The suggestions below are based on
% heuristics and should not be seen as complete nor as a guarantee for
% success.
%
% Weight change:    Displays the changing weights during training. If they 
%                   get stuck on a value before the end of the training 
%                   (straight lines), they usually got stuck in a suboptimal 
%                   configuration and the detection performance of the 
%                   network is low.
%                   Try:  - Re-run the training procedure (weights are 
%                           initialized randomly, thus, rerunning can lead
%                           to a different outcome. 
%                         - Increase training data set. 
% Network output:   Displays the network output of the training samples
%                   (black) and the target output (red). The target output
%                   is 1 for the positive examples pos_bins and -1 for the
%                   negative examples collected from the spectrograms
%                   excluding the positive examples and their immediate 
%                   surroundings (defined by 'num_ignore').
%                   If the network output never reaches 1 for the positive
%                   examples (red line = 1), try: 
%                   -   Decreasing the proportion of negative examples 
%                       by decreasing 'neg_example_factor'.
%                   -   Make sure there are no false positives in pos_bins.
%                   If the network output is above -1 for many negative
%                   examples (red line = -1), try:
%                   -   Increase the proportion of negative examples by
%                       increasing 'neg_example_factor'.
%                   In both cases it might help:
%                   -   Change the training window 'win' to a more easily
%                       detectable part of the song
%                   -   Increase the window size 'win' (note: this slows
%                       down the network)
%                   -   Re-run the training procedure (might always help!)
%
% The two figures only give a first impression (and a sense of what can be
% improved). The best way to visualize how good the network can detect the
% unique point is by visualizing its output for different example files. 

% Browse through different examples: 
net_plot_for_song_count(SPEC,pos_bins,[],net,0.5,gID);
% The network output (blue line, bottom panel) should be -1 everywhere 
% except shortly after the positive bins (marked in white on top of the 
% spectrogram, top panel) where the network output should go to +1. The bins 
% where the network output crosses the threshold (red line, set to 0.5) will 
% correspond to the detection points det_bins after applying the network 
% (see below). 


%% apply the two-layer neural network:

% Load another day of the same bird to apply the network for song count
load('SPEC-b14g16-day2.mat')

% Apply the network: 
% Run the function below. In the displayed figure select a threshold for 
% detection and press enter to run for all spectrograms.
det_bins = net_apply_for_song_count(SPEC, net);

% Plot trainging data pos_bins (provided but not used for training in this 
% example) and detection points det_bins calculated by the network and 
% browse through different example spectrograms using the button above the 
% spectrogram. The detection points should always follow the provided  
% training data points. If there are many more detection points than 
% training data points, there are many false positives. If there are fewer  
% detection points than trainging data points there are false negatives.   
net_plot_for_song_count(SPEC,pos_bins,det_bins,net,0.5,gID);


%% count song numbers

% compare detected syllables (det_bins) with provided trainging syllables 
% (pos_bins).
% Number of "true" target syllalbles = 
N_true = length(cell2mat(pos_bins))
% Number of estimated target syllalbles =
N_estimated = length(cell2mat(det_bins))
% The estimated number of target syllable deviates by (in %):
disp(['Estimated song number is wrong by ',num2str((N_estimated-N_true)/N_true*100,2),'%'])
