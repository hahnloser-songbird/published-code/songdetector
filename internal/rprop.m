% rprop

function [weights_new, gamma] = rprop (weights_old, gamma, d_weight, d_weight_old, nu_min, nu_max, nu_faster, nu_slower)
if nargin < 5
    nu_min = 1e-6; % does only make sense for a given space
    nu_max = 50; % does only make sense for a given space
    nu_faster = 1.2;
    nu_slower = .5;
end
% did the derivatives change the direction? did the sign change?
gamma_sign = sign(d_weight.*d_weight_old);
% speed up where gradients have same direction, slow down otherwise
gamma(gamma_sign > 0) = min(gamma(gamma_sign > 0)*nu_faster,nu_max);
gamma(gamma_sign < 0) = max(gamma(gamma_sign < 0)*nu_slower,nu_min);
% derivative positive (increasing error) -> decrease weight
weights_new = weights_old - gamma.*sign(d_weight);
% if any(isnan(weights_new))
%     disp('shit')
% end
end