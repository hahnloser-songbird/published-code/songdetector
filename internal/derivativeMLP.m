% calculate the derivatives for the backpropagation step of the multi-layer
% perceptron. the cost function is C = 1/2(Z-target)^2, where 
% Z = W2 * tansig(W2 * X + B1) + B2

function [dW1, dB1, dW2, dB2] = derivativeMLP(pattern, target, W1, B1, W2, B2)

[Z, ~ , HL_tansig, HL_d_tansig] = evalMLP(pattern, W1, B1, W2, B2);
dB2 = (Z-target);
dW2 = (Z-target) * HL_tansig;
dB1 = (Z-target) * (W2' .* HL_d_tansig);
dW1 =  dB1 * pattern';

end